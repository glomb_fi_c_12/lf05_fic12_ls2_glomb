/**
 *
 * Ladung Klasse
 *
 * @author Lukas Glomb
 * @since 23.03.2022
 * @version 1.0.1
 *
 */

public class Ladung {

    private String bezeichnung;
    private int menge;

    public Ladung(String bezeichnung, int menge) {
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    /**
     *
     * @return String
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     *
     * @param bezeichnung
     */
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    /**
     *
     * @return int
     */
    public int getMenge() {
        return menge;
    }

    /**
     *
     * @param menge
     */
    public void setMenge(int menge) {
        this.menge = menge;
    }

    @Override
    public String toString(){
        return String.format("%-30s%-30s%10s", "Ladung",  this.bezeichnung, this.menge);
    }

}

