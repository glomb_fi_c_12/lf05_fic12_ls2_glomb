public class RaumschiffTest {

    public static void main (String [] args){
        Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
        Ladung k1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung k2 = new Ladung("Bath'leth Klingonen Schwert", 200);
        klingonen.addLadung(k1);
        klingonen.addLadung(k2);
        klingonen.zustandRaumschiff();
        klingonen.ladungsverzeichnisAusgeben();
        klingonen.ladungsverzeichnisAufraeumen();

        Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IKS Khazara");
        Ladung r1 = new Ladung("Borg Schrott", 5);
        Ladung r2 = new Ladung("Rote Materie", 2);
        Ladung r3 = new Ladung("Plasma-Waffe", 50);
        romulaner.addLadung(r1);
        romulaner.addLadung(r2);
        romulaner.addLadung(r3);
        romulaner.zustandRaumschiff();
        romulaner.ladungsverzeichnisAusgeben();
        romulaner.ladungsverzeichnisAufraeumen();



        Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
        Ladung v1 = new Ladung("Forschungssonde", 35);
        Ladung v2 = new Ladung("Photonentorpedo", 3);

        vulkanier.addLadung(v1);
        vulkanier.addLadung(v2);
        vulkanier.zustandRaumschiff();
        vulkanier.ladungsverzeichnisAusgeben();
        vulkanier.ladungsverzeichnisAufraeumen();
    }
}
