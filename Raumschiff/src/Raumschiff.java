/**
 *
 * Raumschiff Klasse
 *
 * @author Lukas Glomb
 * @since 23.03.2022
 * @version 1.0.1
 *
 */

import java.util.ArrayList;
import java.util.Arrays;

public class Raumschiff {

    private int photonentorpedoAnzahl; /* Attribute */
    private float energieversorgungInProzent;
    private float schildeInProzent;
    private float huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private float androidenAnzahl;
    private String schiffsname;
    private ArrayList<String> broadcastKommunikator = new ArrayList<>();
    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<>();


    public Raumschiff(){}


    public Raumschiff(int photonentorpedoAnzahl /* Parameter */, float energieversorgungInProzent, float schildeInProzent, float huelleInProzent, int lebenserhaltungssystemeInProzent, float androidenAnzahl, String schiffsname) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = schildeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.androidenAnzahl = androidenAnzahl;
        this.schiffsname = schiffsname;
    }

    /**
     *
     * @return int
     */
    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }


    /**
     *
     * @param photonentorpedoAnzahl
     */
    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = /* Zuweisung */ photonentorpedoAnzahl;
    }

    /**
     *
     * @return float
     */
    public float getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    /**
     *
     * @param energieversorgungInProzent
     */
    public void setEnergieversorgungInProzent(float energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    /**
     *
     * @return float
     */
    public float getSchildeInProzent() {
        return schildeInProzent;
    }

    /**
     *
     * @param schildeInProzent
     */
    public void setSchildeInProzent(float schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }

    /**
     *
     * @return float
     */
    public float getHuelleInProzent() {
        return huelleInProzent;
    }

    /**
     *
     * @param huelleInProzent
     */
    public void setHuelleInProzent(float huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    /**
     *
     * @return int
     */
    public int getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    /**
     *
     * @param lebenserhaltungssystemeInProzent
     */
    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }

    /**
     *
     * @return float
     */
    public float getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    /**
     *
     * @param androidenAnzahl
     */
    public void setAndroidenAnzahl(float androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    /**
     *
     * @return String
     */
    public String getSchiffsname() {
        return schiffsname;
    }

    /**
     *
     * @param schiffsname
     */
    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    /**
     *
     * @param neueLadung
     */
    public void addLadung(Ladung neueLadung){
        this.ladungsverzeichnis.add(neueLadung);
    }

    /**
     *
     * @param r
     */
    public void photonentorpedoSchiessen(Raumschiff r){
        return;
    }

    /**
     *
     * @param r
     */
    public void phaserkanoneSchiessen(Raumschiff r){
        return;
    }

    /**
     *
     * @param r
     */
    private void treffer (Raumschiff r){
        return;
    }

    /**
     *
     * @param message
     */
    public void nachrichtAnAlle(String message){
        return;
    }

    /**
     *
     * @return ArrayList<String>
     */
    public ArrayList<String> eintraegeLogbuchZurueckgeben (){
        return new ArrayList<>();
    }

    /**
     *
     * @param anzahlTorpedos
     */
    public void photonentorpedosLaden(int anzahlTorpedos){
        return;
    }

    /**
     *
     * @param schutzschild
     * @param energieversorgung
     * @param schiffshuelle
     * @param anzahlDroiden
     */
    public void reparaturDurchfuehren(boolean schutzschild, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden){
        return;
    }

    public void zustandRaumschiff(){
        System.out.println("Name: "+schiffsname);
        System.out.println("Photonentorpedoanzahl: "+photonentorpedoAnzahl);
        System.out.println("Energieversorgung: "+energieversorgungInProzent);
        System.out.println("Schilde: "+schildeInProzent);
        System.out.println("Huelle: "+huelleInProzent);
        System.out.println("Lebenserhaltungssysteme: "+lebenserhaltungssystemeInProzent);
        System.out.println("Androidenanzahl: "+androidenAnzahl);
        System.out.println();
        return;
    }

    public void ladungsverzeichnisAusgeben(){
        for (final Ladung ladung : this.ladungsverzeichnis){
            System.out.println(ladung.toString());
        }
        return;
    }

    public void ladungsverzeichnisAufraeumen(){
        this.ladungsverzeichnis.removeIf(ladung -> ladung.getMenge() <= 0);
    }
}